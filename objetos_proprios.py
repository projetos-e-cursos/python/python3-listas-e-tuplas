class ContaCorrente:
    def __init__(self, codigo):
        self.codigo = codigo
        self.saldo = 0

    def deposita(self, valor):
        self.saldo += valor

    def __str__(self):
        return f'[>>Codigo {self.codigo}, saldo: {self.saldo}'


conta_gui = ContaCorrente(47685)
conta_gui.deposita(15)
conta_juu = ContaCorrente(123456)

contas = [conta_gui, conta_juu]

for conta in contas:
    print(conta)
