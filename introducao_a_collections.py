idade1 = 39
idade2 = 25
idade3 = 44
idade4 = 19

idades = [39, 30, 27, 18]

# print(type(idades))

# print(idades[0])

idades.append(15)

idades.remove(27)

idades.append(15)
# print(idades)

idades.remove(15)
# print(idades)

# idades.clear()

# print(idades)

# print(28 in idades)

# print(15 in idades)

if 15 in idades:
    idades.remove(15)

# print(idades)

idades.insert(2, 15)

# print(idades)

idades = [20, 39, 18]

idades.extend([27, 19])

# print(idades)

# for idade in idades:
#     print(idade + 1)

# idades_no_ano_que_vem = []
# for idade in idades:
#     idades_no_ano_que_vem.append(idade + 1)

# print(idades_no_ano_que_vem)

idades_no_ano_que_vem = [(idade + 1) for idade in idades]
# print(idades_no_ano_que_vem)

idades_filtradas = [idade for idade in idades if idade > 21]
# print(idades_filtradas)


def faz_processamento_de_visualizacao(lista=None):
    if not lista:
        lista = list()
    # print(len(lista))
    lista.append(13)


faz_processamento_de_visualizacao(idades)
# print(idades)


