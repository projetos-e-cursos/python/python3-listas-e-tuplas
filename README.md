# Python3 - Listas e Tuplas

### Conceitos abordados:
- O que são Listas;
- append();
- remove();
- extend();
- insert();
- List comprehension;
- type();
- Percorrer e filtrar listas;
- Criar função com valores padrões;
- Lidar com referências;
- O que são Tuplas;
- Tupla de listas;
- Diferença entre programação orientada a objetos e funcional;
- Lista de tuplas;
- Array no Python;
- Numpy;
- range();
- enumerate();
- desempacotamento automatico de tuplas;
- sorted(idades)
- reversed()
- sorted(lista, reverse=True)
- sort()
- __eq\_\_;
- \_\_it__;
- attrgetter;
- total_ordering.